#!/usr/bin/bash

version=`cat VERSION`
mkdir "soundtunnel-$version"
cp -r config "soundtunnel-$version"
cp -r src "soundtunnel-$version"
cp CMakeLists.txt "soundtunnel-$version"

tar czvf "soundtunnel-$version.tar.gz" "soundtunnel-$version"

rm -rf "soundtunnel-$version"

