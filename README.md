# Soundtunnel

Soundtunnel allows to send IP traffic between two computers via sound.


Installation
------------

If you are using Arch Linux, just install the AUR package `soundtunnel`. Make sure to use the `liquid-dsp-quiet-devel-git` package as the dependency, as it contains modifications required by `quiet`.

Installation can alternatively be done via `cmake`.

Dependencies
-----------
The dependencies for `soundtunnel` are largely based on the `quiet` library
* [Quiet](https://github.com/quiet/quiet)
* [libconfig](https://github.com/hyperrealm/libconfig)
* [Liquid DSP](https://github.com/quiet/liquid-dsp/tree/devel) Be sure to work from the devel branch
* [libfec](https://github.com/quiet/libfec) (optional but strongly recommended)
* [Jansson](https://github.com/akheron/jansson)
* [libsndfile](http://www.mega-nerd.com/libsndfile/) (optional)
* [PortAudio](http://www.portaudio.com/) (optional)


How to use
----------

WIP: This section will be expanded soon.

