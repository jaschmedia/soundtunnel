#ifndef SOUNDTUNNEL_UTIL_H
#define SOUNDTUNNEL_UTIL_H

void sig_handler(int signal);
void exit_function(int exit_code);

#endif //SOUNDTUNNEL_UTIL_H
