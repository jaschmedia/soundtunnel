#include <netinet/in.h>
#include <net/if.h>
#include <libnet.h>
#include "audio_tunnel.h"
#include "util.h"
#include <portaudio.h>


#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"


void *read_audio(void *read_args) {
    int const max_frame_len = 16384;
    audio_in_args *args = read_args;
    int frame_termination_occ = 0;
    int frame_size = 0;
    char *frame = calloc(max_frame_len, sizeof(char));
    // Assuring the frame buffer was correctly allocated.
    if (frame == NULL) {
        // TODO(jan) - read_audio: Make sure error handling is correct.
        perror("calloc - read frame buffer");
        exit_function(1);
    }
    while (true) {
        ssize_t read = quiet_portaudio_decoder_recv(dec, args->write_buffer, args->write_buffer_size);
        if (read > 0) {
            if (read == 2 && args->write_buffer[0] == '\r' && args->write_buffer[1] == '\n') {
                frame_termination_occ++;
            } else {
                // Assuring we are within our buffer sizes.
                if (frame_size >= max_frame_len || (frame_size + read) >= max_frame_len) {
                    // TODO(jan) - read_audio: Make sure error handling is correct.
                    fprintf(stderr, "Frame size violation! Max size: %i; Current size: %i; Add length: %i\n",
                            max_frame_len, frame_size, read);
                    exit_function(1);
                }
                memcpy(&frame[frame_size], args->write_buffer, read);
                frame_size += read;
            }
            if (frame_termination_occ == 2) {

                write(args->fd, frame, frame_size);
                if (args->cfg->debug) {
                    fprintf(debout, "IN  (%d) ", frame_size);
                    for (int j = 0; j < frame_size; ++j) {
                        fprintf(debout, "%02hhX", frame[j]);
                    }
                    fprintf(debout, "\n");
                }

                // TODO(jan): Assure frame is correctly zero'd
                // Zero out buffer
                memset(frame, 0, frame_size);
                // Set frame size back to 0
                frame_size = 0;
                frame_termination_occ = 0;
            } else {
            }
        }
    }

}

int tun_alloc(soundtunnel_config *cfg, int flags) {
    if (cfg->debug) {
        fprintf(debout, "Starting TUN setup.\n");
    }
    struct ifreq ifr;
    int fd, err;
    char *clonedev = "/dev/net/tun";

    if (cfg->debug)
        fprintf(debout, "Attempting to open TUN device.\n");
    if ((fd = open(clonedev, O_RDWR)) < 0) {
        perror("Opening /dev/net/tun");
        return fd; // FIXME(jan) tun_alloc: rework error handling
    }

    memset(&ifr, 0, sizeof(ifr));

    ifr.ifr_flags = flags;

    if (*cfg->if_name) {
        if (cfg->debug)
            fprintf(debout, "Copying TUN name!\n");
        strncpy(ifr.ifr_name, cfg->if_name, IFNAMSIZ);
    }

    if (cfg->debug)
        fprintf(debout, "Attempting to setup TUN device with name %s\n", cfg->if_name);
    if ((err = ioctl(fd, TUNSETIFF, (void *) &ifr)) < 0) {
        perror("ioctl(TUNSETIFF)");
        close(fd);
        return err; // FIXME(jan) tun_alloc: rework error handling
    }

    if (cfg->debug)
        fprintf(debout, "TUN device set up with file descriptor: %i\n", fd);

    strncpy(cfg->if_name, ifr.ifr_name, IFNAMSIZ - 1);

    if (cfg->debug)
        fprintf(debout, "Returning TUN file descriptor.\n");
    return fd;
}

int setup_link(soundtunnel_config *cfg) {
    if (cfg->debug)
        fprintf(debout, "Starting to set up TUN link.\n");
    struct ifreq ifr;
    struct sockaddr_in addr;
    int stat, s;

    memset(&addr, 0, sizeof(addr));

    s = socket(AF_INET, SOCK_DGRAM, 0);

    // Check the current set address for the device
    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, cfg->if_name, IFNAMSIZ-1);

    if (cfg->debug)
        fprintf(debout, "Checking for current IP on TUN device.\n");
    if(ioctl(s, SIOCGIFADDR, &ifr) == -1) {
        perror("ioctl(SIOCGIFADDR)");
//        exit_function(2);
        // FIXME(jan) setup_link: implement error handling
    }

    addr = *(struct sockaddr_in *) &ifr.ifr_addr;
    if (cfg->debug) {
        fprintf(debout, "Current IP Address: %s\n", inet_ntoa(addr.sin_addr));
        fprintf(debout, "Configured IP Address: %s\n", inet_ntoa(cfg->in_addr.sin_addr));
    }

    if (addr.sin_addr.s_addr != cfg->in_addr.sin_addr.s_addr) {
        memset(&ifr, 0, sizeof(ifr));
        strncpy(ifr.ifr_name, cfg->if_name, IFNAMSIZ-1);

        ifr.ifr_addr = * (struct sockaddr *) &cfg->in_addr;

        if(ioctl(s, SIOCSIFADDR, &ifr) == -1) {
            perror("ioctl(SIOCSIFADDR)");
            exit_function(2);
            // FIXME(jan) setup_link: implement error handling
        }
    }

    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, cfg->if_name, IFNAMSIZ-1);

    if(ioctl(s, SIOCGIFNETMASK, &ifr) == -1) {
        perror("ioctl(SIOCGIFNETMASK)");
//        exit_function(2);
        // FIXME(jan) setup_link: implement error handling
    }

    addr = *(struct sockaddr_in *) &ifr.ifr_netmask;
    if (cfg->debug) {
        fprintf(debout, "Current netmask: %s\n", inet_ntoa(addr.sin_addr));
        fprintf(debout, "Configured netmask: %s\n", inet_ntoa(cfg->netmask.sin_addr));
    }

    if (addr.sin_addr.s_addr != cfg->netmask.sin_addr.s_addr) {
        ifr.ifr_netmask = * (struct sockaddr *) &cfg->netmask;

        if(ioctl(s, SIOCSIFNETMASK, &ifr) == -1) {
            perror("ioctl(SIOCSIFNETMASK)");
            exit_function(2);
            // FIXME(jan) setup_link: implement error handling
        }
    }

    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, cfg->if_name, IFNAMSIZ-1);

    if(ioctl(s, SIOCGIFFLAGS, &ifr) == -1) {
        perror("ioctl(SIOCGIFFLAGS)");
//        exit_function(2);
        // FIXME(jan) setup_link: implement error handling
    }

    if (cfg->debug) {
        fprintf(debout, "Flags: %x\n", ifr.ifr_flags);
        if (ifr.ifr_flags & IFF_UP)
            fprintf(debout, "IFF_UP\n");
        if (ifr.ifr_flags & IFF_BROADCAST)
            fprintf(debout, "IFF_BROADCAST\n");
        if (ifr.ifr_flags & IFF_DEBUG)
            fprintf(debout, "IFF_DEBUG\n");
        if (ifr.ifr_flags & IFF_LOOPBACK)
            fprintf(debout, "IFF_LOOPBACK\n");

        if (ifr.ifr_flags & IFF_POINTOPOINT)
            fprintf(debout, "IFF_POINTOPOINT\n");
        if (ifr.ifr_flags & IFF_NOTRAILERS)
            fprintf(debout, "IFF_NOTRAILERS\n");
        if (ifr.ifr_flags & IFF_RUNNING)
            fprintf(debout, "IFF_RUNNING\n");
        if (ifr.ifr_flags & IFF_NOARP)
            fprintf(debout, "IFF_NOARP\n");

        if (ifr.ifr_flags & IFF_PROMISC)
            fprintf(debout, "IFF_PROMISC\n");
    }

    if (ifr.ifr_flags != 0x10d1) {
        ifr.ifr_flags = IFF_UP | IFF_POINTOPOINT | IFF_RUNNING | IFF_NOARP;

        if(ioctl(s, SIOCSIFFLAGS, &ifr) == -1) {
            perror("ioctl(SIOCSIFFLAGS)");
            exit_function(2);
            // FIXME(jan) setup_link: implement error handling
        }
    }
}

void run(soundtunnel_config *cfg, int fd, quiet_encoder_options *encodeopt, quiet_decoder_options *decodeopt) {
    signal(SIGINT, sig_handler);
    signal(SIGTERM, sig_handler);

    PaError err = Pa_Initialize();
    if (err != paNoError) {
        fprintf(stderr, "failed to initialize port audio, %s\n", Pa_GetErrorText(err));
        return;
    }

    //OUTPUT through audio
    PaDeviceIndex outDevice;
    if (cfg->outDevice == -1) {
        outDevice = Pa_GetDefaultOutputDevice();
    } else {
        outDevice = cfg->outDevice;
    }
    const PaDeviceInfo *outDeviceInfo = Pa_GetDeviceInfo(outDevice);
    if (cfg->debug) {
        fprintf(debout, "Audio Out Device: (%i) %s\n", outDevice, outDeviceInfo->name);
    }

    double out_rate = outDeviceInfo->defaultSampleRate;
    PaTime out_latency = outDeviceInfo->defaultLowOutputLatency;
    size_t out_buffer_size = 16384;
    enc = quiet_portaudio_encoder_create(encodeopt, outDevice, out_latency, out_rate, out_buffer_size);
    uint8_t *read_buffer = malloc(out_buffer_size * sizeof(uint8_t));

    //INPUT through audio
    PaDeviceIndex inDevice;
    if (cfg->inDevice == -1) {
        inDevice = Pa_GetDefaultInputDevice();
    } else {
        inDevice = cfg->inDevice;
    }
    const PaDeviceInfo *deviceInfo = Pa_GetDeviceInfo(inDevice);
    if (cfg->debug) {
        fprintf(debout, "Audio In Device: (%i) %s\n", inDevice, deviceInfo->name);
    }
    double in_rate = deviceInfo->defaultSampleRate;
    PaTime in_latency = deviceInfo->defaultLowInputLatency;
    dec = quiet_portaudio_decoder_create(decodeopt, inDevice, in_latency, in_rate);
    quiet_portaudio_decoder_set_blocking(dec, 0, 0);
    size_t write_buffer_size = 16384;
    uint8_t *write_buffer = malloc(write_buffer_size * sizeof(uint8_t));

    pthread_t thread_id;

    audio_in_args args;
    args.dec = dec;
    args.write_buffer = write_buffer;
    args.write_buffer_size = write_buffer_size;
    args.fd = fd;
    args.cfg = cfg;

    if(pthread_create(&thread_id, NULL, read_audio, (void *) &args)) {
        perror("pthread_create()");
        exit(1);
    }

    while (true) {
        int nread = read(fd, read_buffer, out_buffer_size);
        if (nread < 0) {
            perror("Reading from interface");
            close(fd);
            exit(1);
        }
        /* Do whatever with the data */
        size_t frame_len = quiet_portaudio_encoder_get_frame_len(enc);

        // Send the packet via libquiet, splitting it into appropriate sizes
        for (int i = 0; i < nread; i += frame_len) {
            frame_len = (frame_len > (nread - i)) ? (nread - i) : frame_len;
            quiet_portaudio_encoder_send(enc, read_buffer + i, frame_len);
        }
        if (cfg->debug) {
            fprintf(debout, "OUT (%d) ", nread);
            for (int j = 0; j < nread; ++j) {
                fprintf(debout, "%02hhX", read_buffer[j]);
            }
            fprintf(debout, "\n");
        }
        // Send two CRLF to signal receiver the end of packet.
        quiet_portaudio_encoder_send(enc, (uint8_t *) frame_terminator, 2);
        quiet_portaudio_encoder_send(enc, (uint8_t *) frame_terminator, 2);

    }


}


void setup_quiet(soundtunnel_config *cfg, int fd) {
    quiet_encoder_options *encodeopt = quiet_encoder_profile_filename(cfg->profiles_file, cfg->enc_profile);
    quiet_decoder_options *decodeopt = quiet_decoder_profile_filename(cfg->profiles_file, cfg->dec_profile);
    if (!encodeopt) {
        fprintf(stderr, "failed to read enc profile %s from %s\n", cfg->enc_profile, cfg->profiles_file);
        exit(1);
    }
    if (!decodeopt) {
        fprintf(stderr, "failed to read dec profile %s from %s\n", cfg->dec_profile, cfg->profiles_file);
        exit(1);
    }

    run(cfg, fd, encodeopt, decodeopt);
}

int main(int argc, char **argv) {
    debout = stdout;
    {
        int numDevices;
        PaError err = Pa_Initialize();
        numDevices = Pa_GetDeviceCount();
        if( numDevices < 0 )
        {
            fprintf(stderr, "ERROR: Pa_CountDevices returned 0x%x\n", numDevices );
            err = numDevices;
        }

        const   PaDeviceInfo *deviceInfo;
        for(int i=0; i<numDevices; i++ )
        {
            deviceInfo = Pa_GetDeviceInfo( i );
            fprintf(debout, "Name (%i): %s\n", i, deviceInfo->name);
        }

    }
    char *default_path = "/etc/soundtunnel/soundtunnel.cfg";

    soundtunnel_config cfg;
    int tun_fd;
    int flags = IFF_TUN;

    soundtunnel_config cli_cfg;

    char *cli_config_file = NULL;


    // Booleans for what has been set
    struct {
        int interface_set;
        int addr_set;
        int netmask_set;
        int profile_list_set;
        int enc_set;
        int dec_set;
        int debug_set;
    } cli_set;

    cli_set.interface_set = 0;
    cli_set.addr_set = 0;
    cli_set.netmask_set = 0;
    cli_set.profile_list_set = 0;
    cli_set.enc_set = 0;
    cli_set.dec_set = 0;
    cli_set.debug_set = 0;

    int c;
    int digit_optind = 0;

    cli_cfg.debug = 0;

    while (1) {
        int this_option_optind = optind ? optind : 1;
        int option_index = 0;

        static struct option long_options[] = {
                {"config", required_argument, 0, 'c'},
                {"interface", required_argument, 0, 't'},
                {"ip-addr", required_argument, 0, 'a'},
                {"netmask", required_argument, 0, 'n'},
                {"profile-list", required_argument, 0, 'p'},
                {"enc-profile", required_argument, 0, 'o'},
                {"dec-profile", required_argument, 0, 'i'},
                {"debug", no_argument, 0, 'd'},
                {0, 0, 0, 0}
        };

        c = getopt_long(argc, argv, "c:t:a:n:p:o:i:d", long_options, &option_index);

        if (c == -1) {
            break;
        }

        int str_len;

        switch (c) {
            case 'c':
                str_len = strlen(optarg);
                cli_config_file = calloc(str_len + 1, sizeof(char));
                strncpy(cli_config_file, optarg, str_len);
                break;
            case 't':
                strncpy(cli_cfg.if_name, optarg, IFNAMSIZ - 1);
                cli_set.interface_set = 1;
                break;
            case 'a':
                cli_cfg.in_addr.sin_family = AF_INET;
                inet_aton(optarg, &cli_cfg.in_addr.sin_addr);
                cli_set.addr_set = 1;
                break;
            case 'n':
                cli_cfg.netmask.sin_family = AF_INET;
                inet_aton(optarg, &cli_cfg.netmask.sin_addr);
                cli_set.netmask_set = 1;
                break;
            case 'p':
                cli_cfg.profiles_file = calloc(strlen(optarg) + 1, sizeof(char));
                strncpy(cli_cfg.profiles_file, optarg, strlen(optarg));
                cli_set.profile_list_set = 1;
                break;
            case 'o':
                cli_cfg.enc_profile = calloc(strlen(optarg) + 1, sizeof(char));
                strncpy(cli_cfg.enc_profile, optarg, strlen(optarg));
                cli_set.enc_set = 1;
                break;
            case 'i':
                cli_cfg.dec_profile = calloc(strlen(optarg) + 1, sizeof(char));
                strncpy(cli_cfg.dec_profile, optarg, strlen(optarg));
                cli_set.dec_set = 1;
                break;
            case 'd':
                cli_cfg.debug = 1;
                cli_set.debug_set = 1;
                break;
            default:
                break;
        }


    }

    if (cli_cfg.debug) {
        fprintf(debout, "Finished CLI parsing\n");
    }

    char *cfg_file_path;

    if (cli_config_file != NULL) {
        cfg_file_path = cli_config_file;
    } else {
        cfg_file_path = default_path;
    }


    if (cli_cfg.debug) {
        fprintf(debout, "Reading in config file!\n");
        fprintf(debout, "File that will be read: %s\n", cfg_file_path);
    }
    config_t file_cfg;

    config_init(&file_cfg);

    if(! config_read_file(&file_cfg, cfg_file_path))
    {
        fprintf(stderr, "%s:%d - %s\n", config_error_file(&file_cfg),
                config_error_line(&file_cfg), config_error_text(&file_cfg));
        config_destroy(&file_cfg);
        return(EXIT_FAILURE);
    }

    if(config_lookup_bool(&file_cfg, "debug", &cfg.debug)) {
        fprintf(debout, "[CONFIG] - Debugging: %i\n", cfg.debug);
    } else {
        fprintf(stderr, "No 'debug' setting in configuration file.\n");
    }

    if (cli_set.debug_set) {
        cfg.debug = 1;
    }

    const char *if_name_conf;
    if(config_lookup_string(&file_cfg, "device_name", &if_name_conf)) {
        if (cfg.debug)
            fprintf(debout, "[CONFIG] - Device name: %s\n", if_name_conf);
        strncpy(cfg.if_name, if_name_conf, IFNAMSIZ - 1);
    } else {
        fprintf(stderr, "No 'device_name' setting in configuration file.\n");
    }

    const char *cfg_file_ip_addr;
    if(config_lookup_string(&file_cfg, "ip_addr", &cfg_file_ip_addr)) {
        memset(&cfg.in_addr, 0, sizeof(cfg.in_addr));
        cfg.in_addr.sin_family = AF_INET;
        if(inet_aton(cfg_file_ip_addr, &cfg.in_addr.sin_addr)) {
            if (cfg.debug)
                fprintf(debout, "[CONFIG] - Ip addr: %s\n", cfg_file_ip_addr);
        } else {
            fprintf(stderr, "Invalid ip addr provided.\n");
        }
    }

    const char *cfg_file_netmask;
    if(config_lookup_string(&file_cfg, "netmask", &cfg_file_netmask)) {
        memset(&cfg.netmask, 0, sizeof(cfg.netmask));
        cfg.netmask.sin_family = AF_INET;
        if(inet_aton(cfg_file_netmask, &cfg.netmask.sin_addr)) {
            if (cfg.debug)
                fprintf(debout, "[CONFIG] - Netmask: %s\n", cfg_file_netmask);
        } else {
            fprintf(stderr, "Invalid netmask provided.\n");
        }
    }

    if(config_lookup_string(&file_cfg, "enc-profile", &cfg.enc_profile)) {
        if (cfg.debug)
            fprintf(debout, "[CONFIG] - Encoder profile: %s\n", cfg.enc_profile);
    } else {
        fprintf(stderr, "No 'enc-profile' setting in configuration file.\n");
    }

    if(config_lookup_string(&file_cfg, "dec-profile", &cfg.dec_profile)) {
        if (cfg.debug)
            fprintf(debout, "[CONFIG] - Decoder profile: %s\n", cfg.dec_profile);
    } else {
        fprintf(stderr, "No 'dec-profile' setting in configuration file.\n");
    }

    if(config_lookup_string(&file_cfg, "profiles-file", &cfg.profiles_file)) {
        if (cfg.debug)
            fprintf(debout, "[CONFIG] - Profiles file: %s\n", cfg.profiles_file);
    } else {
        fprintf(stderr, "No 'profiles-file' setting in configuration file.\n");
    }

    if(config_lookup_int(&file_cfg, "output-device", &cfg.outDevice)) {
        if (cfg.debug)
            fprintf(debout, "[CONFIG] - Output Device: %i\n", cfg.outDevice);
    } else {
        fprintf(stderr, "No 'output-device' setting in configuration file.\n");
    }

    if(config_lookup_int(&file_cfg, "input-device", &cfg.inDevice)) {
        if (cfg.debug)
            fprintf(debout, "[CONFIG] - Input Device: %i\n", cfg.inDevice);
    } else {
        fprintf(stderr, "No 'input-device' setting in configuration file.\n");
    }


    // Combine CLI and parsed config

    if (cli_set.debug_set) {
        cfg.debug = 1;
    }

    if (cli_set.interface_set) {
        strncpy(cfg.if_name, cli_cfg.if_name, IFNAMSIZ - 1);
    }

    if (cli_set.addr_set) {
        cfg.in_addr = cli_cfg.in_addr;
    }

    if (cli_set.netmask_set) {
        cfg.netmask = cli_cfg.netmask;
    }

    if (cli_set.profile_list_set) {
        cfg.profiles_file = cli_cfg.profiles_file;
    }

    if (cli_set.enc_set) {
        cfg.enc_profile = cli_cfg.enc_profile;
    }

    if (cli_set.dec_set) {
        cfg.dec_profile = cli_cfg.dec_profile;
    }

    if (cfg.debug) {
        fprintf(debout, "Finalised config!\n");
        fprintf(debout, "[FINAL] - Interface name: %s\n", cfg.if_name);
        fprintf(debout, "[FINAL] - IP Address: %s\n", inet_ntoa(cfg.in_addr.sin_addr));
        fprintf(debout, "[FINAL] - Netmask: %s\n", inet_ntoa(cfg.netmask.sin_addr));
        fprintf(debout, "[FINAL] - Profiles file: %s\n", cfg.profiles_file);
        fprintf(debout, "[FINAL] - Encoding profile: %s\n", cfg.enc_profile);
        fprintf(debout, "[FINAL] - Decoding profile: %s\n", cfg.dec_profile);
    }

    if ((tun_fd = tun_alloc(&cfg, flags | IFF_NO_PI)) < 0) {
        fprintf(stderr, "Cannot connect to tun device: %s!\n", cfg.if_name);
        exit(1);
    }

    setup_link(&cfg);

    setup_quiet(&cfg, tun_fd);

    return 0;
}

#pragma clang diagnostic pop