#ifndef SOUNDTUNNEL_AUDIO_TUNNEL_H
#define SOUNDTUNNEL_AUDIO_TUNNEL_H

#include <stdlib.h>
#include <stdio.h>
#include <net/if.h>
#include <linux/if_tun.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <quiet.h>
#include <portaudio.h>
#include <quiet-portaudio.h>
#include <signal.h>
#include <pthread.h>
#include <libconfig.h>
#include <linux/netdevice.h>

#include "config.h"

static FILE *debout;

static quiet_portaudio_decoder *dec = NULL;
static quiet_portaudio_encoder *enc = NULL;
static char *frame_terminator = "\r\n";


typedef struct {
    quiet_portaudio_decoder *dec;
    uint8_t *write_buffer;
    size_t  write_buffer_size;
    int fd;
    soundtunnel_config *cfg;

} audio_in_args;

#endif //SOUNDTUNNEL_AUDIO_TUNNEL_H
