//
// Created by jasch on 16/05/19.
//

#ifndef SOUNDTUNNEL_CONFIG_H
#define SOUNDTUNNEL_CONFIG_H

#include <netinet/in.h>
#include <net/if.h>
#include <stdlib.h>

typedef struct {
    char if_name[IFNAMSIZ];
    struct sockaddr_in in_addr;
    struct sockaddr_in netmask;

    int is_server;
    char *enc_profile;
    char *dec_profile;

    char *profiles_file;

    int outDevice;
    int inDevice;

    int debug;
} soundtunnel_config;

#endif //SOUNDTUNNEL_CONFIG_H
