#include "audio_tunnel.h"
#include "util.h"


void sig_handler(int signal) {
    printf("SIG HANDLER! %i\n", signal);

    exit_function(0);
}

void exit_function(int exit_code) {
    if (dec) {
        printf("Attempting to close decoder\n");
        quiet_portaudio_decoder_close(dec);
        printf("Closed decoder!\n");
    }
    if (enc) {
        printf("Attempting to close encoder\n");
        quiet_portaudio_encoder_destroy(enc);
        printf("Closed encoder!\n");
    }

    Pa_Terminate();

    exit(exit_code);
}
